Neden ansible ve fazlası

[Ansible Docs1](https://docs.ansible.com/)

[Ansible Docs2](https://www.javatpoint.com/ansible)


[Ansible condition when etc.](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)

[Ansible condition when etc. 2](https://adamtheautomator.com/ansible-when/)

[Ansible lead](http://www.freekb.net/Articles?tag=Ansible/)

[examples_ansible](https://www.middlewareinventory.com/blog/ansible-playbook-example/)

[upgrade withoud kube](https://community.hetzner.com/tutorials/awx-without-kubernetes)

[ansible ci cd](https://der-jd.de/blog/2021/01/02/Using-ansible-with-gitlab-ci/)


[credentials-openshift-baerer token](https://docs.ansible.com/ansible-tower/latest/html/userguide/credentials.html)


---

cd .ssh
ls

cat id_rsa.pub  ## public key


ls -la

cat authorized_keys

To allow remote management, which file on the remote system should include the SSH key for the control node? ## authorized keys,



ansible-playbook orchestration.yml -i myhosts


ansible all -m ping -i myhosts

ansible-playbook change.yml -f 30 -i myhosts

-f (fork) ; 

[performance_tuning](https://www.ansible.com/blog/ansible-performance-tuning)

[fork-serial](https://docs.ansible.com/ansible/latest/user_guide/playbooks_strategies.html)


Which statement is true regarding modern cloud infrastructure? ## Cloud infrastructure usually involves different OSs.

When will you want to set the changed_when conditional to true? ## when a task should be treated as though it caused a change


```
apt:
  name:
  - ntp
  state: present
```

following task to do ## Install the NTP service, only if it is needed.

In an Ansible playbook, what is the impact of setting serial: 1? ## All tasks must complete on one server before moving to the next server.


Why should you enable the strategy: free option? ## for continuation of the play on a host, without waiting for other hosts

Which ad hoc argument flag will you use to set the number of forks to 10? ## -f 10







