mkdir Tower
cd Tower/
git clone https://github.com/ansible/awx.git 
cd awx 
cd installer/

#vi inventory 
-# openshift_host=127.0.0.1:8443 
-# awx_openshift_project=awx 
-# openshift_user=developer 
-# awx_node_port=30083 
+ openshift_host=127.0.0.1:8443 
+ awx_openshift_project=awx 
+ openshift_user=developer 
+ awx_node_port=30001 
+# valid host port range 30000-32767 
+ openshift_password=developer 
# Standalone Docker Install 
-postgres_data_dir=/tmp/pgdocker 
-host_port=80 
+#postgres_data_dir=/tmp/pgdocker 
+#host_port=80 
# Define if you want the image pushed to a registry. The container definition will also use these images 
-# docker_registry=172.30.1.1:5000 
-# docker_registry_repository=awx 
-# docker_registry_username=developer 
+ docker_registry=172.30.1.1:5000 
+ docker_registry_repository=awx 
+ docker_registry_username=developer 

ansible-playbook -i inventory install.yml 

#get the name of the AWX replica set
export RS_AWX = oc get rs|awk  'FNR>1 {print $1}'
oc scale --replicas=0 rs $RS_AWX

#add volume to the replicaset that will be mounted to awx-celery
oc patch rs $RS_AWX -p '{"spec":{"template":{"spec":{"volumes":[{"name":"awxprojectsdata","persistentVolumeClaim":{"claimName":"claim-awx"}}]}}}}'
#mount the volume to awx-celery
oc patch rs $RS_AWX -p '{"spec":{"template":{"spec":{"containers":[{"name":"awx-celery","volumeMounts":[{"mountPath":"/var/lib/awx/projects/","name":"awxprojectsdata"}]}]}}}}'

oc scale --replicas=1 rs $RS_AWX
