## Output

_Powered by Kani Eren_

```
TASK [service_control : start service systemd] *********************************

changed: [ansibleX01] => (item=filebeat)


TASK [Gathering Facts] *********************************************************

[WARNING]: Platform linux on host ansibleX is using the discovered Python
interpreter at /usr/bin/python, but future installation of another Python
interpreter could change this. See https://docs.ansible.com/ansible/2.9/referen
ce_appendices/interpreter_discovery.html for more information.
ok: [ansibleX01]

TASK [service_control : start service systemd] *********************************
changed: [ansibleX01] => (item=filebeat)
TASK [service_control : restart service systemd] *******************************
skipping: [ansibleX01] => (item=filebeat) 
TASK [service_control : stop service systemd] **********************************
skipping: [ansibleX01] => (item=filebeat) 
PLAY RECAP *********************************************************************


ansibleX01              : ok=2    changed=1    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0   
ansibleX02              : ok=2    changed=1    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0   
ansibleX03              : ok=2    changed=1    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0   
```
